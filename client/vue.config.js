module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  devServer: {
    port: 8081,
    proxy: {
      '^/api': {
        target: 'http://localhost:8080',
        pathRewrite: {
          '^/api': '/api'
        },
        changeOrigin: true,
        logLevel: 'debug'
      }
    }
  }
}

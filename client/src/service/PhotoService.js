import axios from "axios";

export default class PhotoService {

    upload(formData) {
        return axios.post('api/photo', formData);
    }

    getAll() {
        return axios.get('api/photo');
    }
}

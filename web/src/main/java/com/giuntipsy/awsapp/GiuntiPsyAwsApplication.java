package com.giuntipsy.awsapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class GiuntiPsyAwsApplication {

	private static final Logger logger = LoggerFactory.getLogger(GiuntiPsyAwsApplication.class);

	public static void main(String[] args) {

		logger.info("Starting application");

		SpringApplication.run(GiuntiPsyAwsApplication.class, args);
	}

}

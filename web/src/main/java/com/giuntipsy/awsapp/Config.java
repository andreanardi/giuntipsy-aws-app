package com.giuntipsy.awsapp;

import com.amazonaws.xray.javax.servlet.AWSXRayServletFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

@Configuration
public class Config {

    @Bean
    public FilterRegistrationBean xrayFilter() {
        final FilterRegistrationBean registration = new FilterRegistrationBean(new AWSXRayServletFilter("sample"));
        registration.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return registration;
    }
}

package com.giuntipsy.awsapp;

import com.amazonaws.xray.spring.aop.XRayEnabled;
import org.springframework.data.jpa.repository.JpaRepository;

@XRayEnabled
public interface PhotoRepository extends JpaRepository<Photo, Long> {

    Photo findByKey(String key);

}

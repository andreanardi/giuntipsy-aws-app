package com.giuntipsy.awsapp;

import com.amazonaws.xray.AWSXRay;
import com.amazonaws.xray.spring.aop.XRayEnabled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.services.s3.presigner.S3Presigner;
import software.amazon.awssdk.services.s3.presigner.model.GetObjectPresignRequest;
import software.amazon.awssdk.services.s3.presigner.model.PresignedGetObjectRequest;

import java.io.IOException;
import java.time.Duration;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@XRayEnabled
@RestController
@RequestMapping("/api/photo")
public class PhotoController {

    private static final Logger logger = LoggerFactory.getLogger(PhotoController.class);

    @Value("${giunti-psy-bucket}")
    private String giuntiPsyBucket;

    @Value("${region}")
    private String regionKey;

    @Autowired
    private PhotoRepository photoRepository;

    @PostMapping
    public ResponseEntity<PutObjectResponse> upload(@RequestParam("file") MultipartFile file) throws IOException {

        logger.info("Upload photo {}", file.getOriginalFilename());

        try {
            AWSXRay.beginSubsegment("s3");
            getS3Client().putObject(PutObjectRequest
                    .builder()
                    .bucket(giuntiPsyBucket)
                    .key(file.getOriginalFilename())
                    .build(), RequestBody.fromBytes(file.getBytes()));
            AWSXRay.endSubsegment();

            Photo photo = photoRepository.findByKey(file.getOriginalFilename());
            if (photo == null) {
                photo = new Photo();
                photo.setKey(file.getOriginalFilename());
            }
            photoRepository.save(photo);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error during upload photo", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping
    public ResponseEntity<List<S3ObjectResponse>> getAll() {

        logger.info("Get all photos from bucket {}", giuntiPsyBucket);

        try {
            S3Client s3 = getS3Client();

            ListObjectsV2Request request = ListObjectsV2Request
                    .builder()
                    .bucket(giuntiPsyBucket)
                    .build();

            List<S3ObjectResponse> response = new ArrayList<>();
            for (S3Object content : s3.listObjectsV2(request).contents()) {

                S3ObjectResponse s3ObjectResponse = new S3ObjectResponse();
                s3ObjectResponse.setName(content.key());
                s3ObjectResponse.setSize(content.size());
                s3ObjectResponse.setLastModified(formatDate(content));
                s3ObjectResponse.setUrl(getPresignedUrl(content.key()));

                Photo photo = photoRepository.findByKey(content.key());
                if (photo != null) {
                    s3ObjectResponse.setLabels(photo.getLabels());
                }

                response.add(s3ObjectResponse);
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error during retrieve photo list", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private String formatDate(S3Object content) {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                .withLocale(Locale.ITALY)
                .withZone(ZoneOffset.ofHours(1));
        return formatter.format(content.lastModified());
    }

    private String getPresignedUrl(String key) {
        Region region = Region.of(this.regionKey);
        S3Presigner presigner = S3Presigner.builder().region(region).build();
        GetObjectRequest getObjectRequest = GetObjectRequest.builder().bucket(giuntiPsyBucket).key(key).build();
        GetObjectPresignRequest getObjectPresignRequest = GetObjectPresignRequest.builder().signatureDuration(Duration.ofMinutes(10)).getObjectRequest(getObjectRequest).build();
        PresignedGetObjectRequest presignedGetObjectRequest = presigner.presignGetObject(getObjectPresignRequest);
        return presignedGetObjectRequest.url().toString();
    }

    private S3Client getS3Client() {
        Region region = Region.of(this.regionKey);
        return S3Client.builder().region(region).build();
    }
}

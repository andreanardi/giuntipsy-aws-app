package com.giuntipsy.awsapp;

import javax.persistence.*;

@Entity
@Table(name = "photo", indexes = {
    @Index(name = "key_index", columnList = "key", unique = true)
})
public class Photo {

    @Id 
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @Column(name = "`key`")
    private String key;

    @Column(name = "labels")
    private String labels;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLabels() {
        return labels;
    }

    public void setLabels(String labels) {
        this.labels = labels;
    }
}
